#pragma once
#include "Entity.h"
#include "ResourceManager.h"
#include <SFML/Graphics.hpp>
#include "Config.h"
#include "CardState.h"


class Card : public Entity
{
public:
	Card(sf::Vector2f position, float sizeFactor, std::string backTexture_key, std::string faceTexture_key, float outlineThickness = cfg::outlineThickness, sf::Color outlineColor = cfg::outlineColor);
	~Card();

	void handleEvent(sf::Event event) override;
	void update(sf::Time elapsedTime) override;
	void render(sf::RenderWindow* window) override;

	const sf::RectangleShape& getBackGraphic();
	const sf::RectangleShape& getFaceGraphic();

private:

	sf::Vector2f position;
	sf::Vector2f size;

	float outlineThickness;
	sf::Color outlineColor;
	
	std::string backTexture_key;
	std::string faceTexture_key;

	sf::RectangleShape backGraphic;
	sf::RectangleShape faceGraphic;

	// STATE DATA SETTERS
	void setSize(float sizeFactor);
	void setBackGraphic();
	void setFaceGraphic();

	// STATE PATTERN CARD STATE
	friend class CardState;
	void changeCardState(CardState* nextState);
	CardState* currentCardState;

};



