#include <SFML/Graphics.hpp>

#include "Engine.h"



int main()
{
	Engine myGame;

	// test start
	sf::Vector2f position(10, 20);
	float size = 50;

	Card card = Card(position, size, "illuminati", "v_for_vendetta");

	myGame.addWorldObject(&card);
	// test end

	myGame.gameLoop();

	return 0;
}

