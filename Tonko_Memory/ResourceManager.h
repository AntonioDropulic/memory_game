#pragma once

#include <unordered_map>
#include <filesystem>
#include <iostream>
#include <string>
#include <memory>

#include <SFML/Graphics.hpp>

// singleton
class ResourceManager
{
public:
	
	static ResourceManager& instance();

	ResourceManager(const ResourceManager& original) = delete;
	ResourceManager &operator=(const ResourceManager& rhs) = delete;

	const sf::Texture& getTexture(std::string key) const;

	void setTexture(std::string key, std::string filepath);
	// populates Textures map with files within given folder
	// keys are set to file names (extension not included)
	void loadTextures(std::string folder);
private:
	// default Resources loaded on instantiation // Resources in project resources folder
	ResourceManager();

	std::unordered_map<std::string, sf::Texture> Textures;
};

