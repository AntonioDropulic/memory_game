#include "ResourceManager.h"
#include "Config.h"


ResourceManager::ResourceManager()
{
	// debug
	std::cout << "Resource Manger constructor"<< std::endl;

	// mgc go to config
	loadTextures(cfg::videoResourcesFolder);
}


ResourceManager & ResourceManager::instance()
{
	static ResourceManager instance;
	return instance;
}


const sf::Texture& ResourceManager::getTexture(std::string key) const
{
	// debug
	std::cout << "Resource manager get texture. " << std::endl;
	std::cout << "Resource manager key accesed: " << key << std::endl;

	return (Textures.at(key));
}



void ResourceManager::setTexture(std::string key, std::string filepath)
{
	sf::Texture texture;
	if (!texture.loadFromFile(filepath))
	{
		std::cout << "Resource Manager Error : Failed to load texture." << std::endl;
	}
	// error checking
	std::cout <<"key: " << key << std::endl;

	Textures.emplace(key, sf::Texture(texture));
}

void ResourceManager::loadTextures(std::string folder)
{
	// debug
	std::cout << "Resource Manager : loadTextures entered. " << std::endl;

	for (auto & filepath : std::experimental::filesystem::directory_iterator(folder))
	{
		std::string filepath_string = filepath.path().string();
		std::string filename_string = filepath.path().filename().replace_extension().string();

		//debug
		std::cout << "loadTextures: " << std::endl << "	filepath: " << filepath_string << "    filename: " << filename_string << std::endl;

		setTexture(filename_string, filepath_string);
	}
		
}

