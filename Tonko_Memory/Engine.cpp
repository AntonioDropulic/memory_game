#include "Engine.h"
#include "Config.h"


Engine::Engine()
{
}


void Engine::gameLoop()
{
	// WINDOW SETUP
	// TODO: fgrd 
	sf::RenderWindow window(sf::VideoMode(1600, 1000), "Memory");
	window.clear(sf::Color::White);

	// CLOCK START
	sf::Clock clock;

	sf::Time lastTime = clock.getElapsedTime();
	sf::Time lag = sf::Time::Zero;

	while (window.isOpen())
	{
		//UPDATE TIMER // 
		sf::Time currentTime = clock.getElapsedTime();
		sf::Time elapsedTime = currentTime - lastTime;
		lastTime = currentTime;

		eventManager(&window);

		lag += elapsedTime;
		while (lag >= cfg::msPerUpdate)
		{
			update(cfg::msPerUpdate);
			lag -= cfg::msPerUpdate;
		}

		render(&window);

	}

}

void Engine::addWorldObject(Entity * entity)
{
	worldObjects.push_back(entity);
}

void Engine::eventManager(sf::RenderWindow * window)
{
	sf::Event event;

	// all world objects are notified of events all events... 
	// make better
	while (window->pollEvent(event))
	{
		switch (event.type)
		{
		case sf::Event::Closed:
			window->close();
			break;
		default:
			for (auto entity : worldObjects)
				entity->handleEvent(event);
			break;
		}
	}
}

void Engine::update(sf::Time elapsedTime)
{
	for (auto entity : worldObjects)
	{
		entity->update(elapsedTime);
	}
}

void Engine::render(sf::RenderWindow* window)
{
	
	window->clear(sf::Color::White);


	for (auto entity : worldObjects)
	{
		entity->render(window);
	}

	window->display();
}

