#include "Card.h"




Card::Card(sf::Vector2f position, float sizeFactor, std::string backTexture_key, std::string faceTexture_key, float outlineThickness, sf::Color outlineColor) 
	: position(position), backTexture_key(backTexture_key), faceTexture_key(faceTexture_key), outlineThickness(outlineThickness), outlineColor(outlineColor)
{
	setSize(sizeFactor);

	// ... possible changes
	setBackGraphic();
	setFaceGraphic();

	// initial state 
	currentCardState = new CardBack;
}


Card::~Card()
{
	delete currentCardState;
}

void Card::handleEvent(sf::Event event)
{
	currentCardState->handleEvent(this, event);
}

void Card::update(sf::Time elapsedTime)
{
	currentCardState->update(this, elapsedTime);
}

void Card::render(sf::RenderWindow * window)
{
	currentCardState->render(this, window);
}

const sf::RectangleShape & Card::getBackGraphic()
{
	return backGraphic;
}

const sf::RectangleShape & Card::getFaceGraphic()
{
	return faceGraphic;
}

void Card::setSize(float sizeFactor)
{
	size = sf::Vector2f(sizeFactor*cfg::cardSizeFactor_X,sizeFactor*cfg::cardSizeFactor_Y);
}

void Card::setBackGraphic()
{
	// board selected variables
	backGraphic.setPosition(position.x, position.y);
	backGraphic.setSize(size);
	backGraphic.setTexture(& ResourceManager::instance().getTexture(backTexture_key));
	// const
	backGraphic.setOutlineColor(outlineColor);
	backGraphic.setOutlineThickness(outlineThickness);
}

void Card::setFaceGraphic()
{
	// board selected variables
	faceGraphic.setPosition(position.x, position.y);
	faceGraphic.setSize(size);
	faceGraphic.setTexture(&ResourceManager::instance().getTexture(faceTexture_key));

	// const
	faceGraphic.setOutlineColor(outlineColor);
	faceGraphic.setOutlineThickness(outlineThickness);
}

void Card::changeCardState(CardState * nextState)
{
	delete currentCardState;
	currentCardState = nextState;
	std::cout << "state change" << std::endl;
}
