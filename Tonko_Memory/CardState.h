#pragma once
#include "Entity.h"
#include "Config.h"
#include <SFML/Graphics.hpp>

// TODO:: REWORK ANIMATIONS

class Card;

class CardState
{
public:
	virtual ~CardState();
	
	virtual void handleEvent(Card* context, sf::Event event) = 0;
	virtual void update(Card* context, sf::Time elapsedTime) = 0;
	virtual void render(Card* context, sf::RenderWindow* window) = 0;

protected:
	void changeState(Card* context, CardState* nextState );
	
};

class CardFace : public CardState
{
public:
	void handleEvent(Card* context, sf::Event event);
	void update(Card* context, sf::Time elapsedTime);
	void render(Card* context, sf::RenderWindow* window);
};

class FaceToBackAnimation : public CardState
{
public:

	void handleEvent(Card* context, sf::Event event);
	void update(Card* context, sf::Time elapsedTime);
	void render(Card* context, sf::RenderWindow* window);
private:
	sf::Time animationTimer;
	sf::Transform currentTransform;
	sf::RectangleShape currentGraphic;
};

class CardBack : public CardState
{
public:
	void handleEvent(Card* context, sf::Event event);
	void update(Card* context, sf::Time elapsedTime);
	void render(Card* context, sf::RenderWindow* window);
};


// constructor, animation timer? 
class BackToFaceAnimation : public CardState
{
	void handleEvent(Card* context, sf::Event event);
	void update(Card* context, sf::Time elapsedTime);
	void render(Card* context, sf::RenderWindow* window);
private:
	sf::Time animationTimer;
	sf::Transform currentTransform;
	sf::RectangleShape currentGraphic;
};
