#pragma once
#include <SFML/Graphics.hpp>

// DEFINES AN INTERFACE FOR GAME OBJECTS
class Entity
{
public:
	virtual ~Entity();

	virtual void handleEvent(sf::Event event) = 0;
	virtual void update(sf::Time elapsedTime) = 0;

	// should be called draw
	virtual void render(sf::RenderWindow* window) = 0;

};

