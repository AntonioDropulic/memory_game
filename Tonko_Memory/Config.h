#pragma once
#include<string>
#include <SFML/Graphics.hpp>

using string = std::string;

namespace cfg
{
	// Resources
	const string videoResourcesFolder = "./videoResources";

	// Card
	const float cardSizeFactor_X = 2.5;
	const float cardSizeFactor_Y = 3.5;

	const float outlineThickness = 2;
	const sf::Color outlineColor = sf::Color::Black;

	const sf::Time animationHalfTime = sf::milliseconds(1000);

	// Game loop 
	// 100 updates per sec
	const sf::Time msPerUpdate = sf::milliseconds(10);
}
