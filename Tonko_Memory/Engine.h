#pragma once
#include "Entity.h"
#include <SFML/Graphics.hpp>

#include "Card.h"
#include<vector>

class Engine
{
public:
	Engine();
	
	void gameLoop();

	void createWorld();
	void addWorldObject(Entity* entity);

private:
	
	// dont forget to reserve memory when creating the vector
	std::vector<Entity*> worldObjects;

	//GAME LOOP FUNCTIONS
	// QUESTION: Handling multiple events, saturation?
	void eventManager(sf::RenderWindow* window);
	// set update to a constant window
	void update(sf::Time elapsedTime);
	void render(sf::RenderWindow* window);

};

