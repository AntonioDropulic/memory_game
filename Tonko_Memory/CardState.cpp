#include "CardState.h"
#include "Card.h"


CardState::~CardState()
{
}

void CardState::changeState(Card * context, CardState * nextState)
{
	context->changeCardState(nextState);
}

// FACE

void CardFace::handleEvent(Card * context, sf::Event event)
{
	if (event.type == sf::Event::MouseButtonPressed)
	{
		if (event.mouseButton.button == sf::Mouse::Left)
		{
			if (context->getFaceGraphic().getGlobalBounds().contains(event.mouseButton.x, event.mouseButton.y))
			{
				changeState(context, new FaceToBackAnimation);
				std::cout << "changing state to FaceToBack Animation" << std::endl;
			}
		}
	}
	
}

void CardFace::update(Card * context, sf::Time elapsedTime)
{
	//Dummy
}

void CardFace::render(Card * context, sf::RenderWindow * window)
{
	window->draw(context->getFaceGraphic());
}

// BACK

void CardBack::handleEvent(Card * context, sf::Event event)
{
	if (event.type == sf::Event::MouseButtonPressed)
	{
		if (event.mouseButton.button == sf::Mouse::Left)
		{
			if (context->getBackGraphic().getGlobalBounds().contains(event.mouseButton.x, event.mouseButton.y))
			{
				changeState(context, new BackToFaceAnimation);
				std::cout << "changing state to BackToFace Animation" << std::endl;
			}
		}
	}
	
}

void CardBack::update(Card * context, sf::Time elapsedTime)
{
	//Dummy
}

void CardBack::render(Card * context, sf::RenderWindow * window)
{
	window->draw(context->getBackGraphic());
}

void FaceToBackAnimation::handleEvent(Card * context, sf::Event event)
{
	//dummy
}

void FaceToBackAnimation::update(Card * context, sf::Time elapsedTime)
{
	animationTimer += elapsedTime;

	if (animationTimer.asMilliseconds() < 2*cfg::animationHalfTime.asMilliseconds())
	{
		if (animationTimer.asMilliseconds() < cfg::animationHalfTime.asMilliseconds())
		{
			// run once
			currentGraphic= context->getFaceGraphic();
		
			currentTransform.scale((cfg::animationHalfTime.asMilliseconds() - animationTimer.asMilliseconds()) / cfg::animationHalfTime.asMilliseconds(), 1);
			
		}
		else
		{
			// run once
			currentGraphic = context->getBackGraphic();

			currentTransform.scale((animationTimer.asMilliseconds() - cfg::animationHalfTime.asMilliseconds()) / cfg::animationHalfTime.asMilliseconds(), 1);
		}
	}
	else
		changeState(context, new CardBack);	
}

void FaceToBackAnimation::render(Card * context, sf::RenderWindow * window)
{
	window->draw(currentGraphic, currentTransform);
}

void BackToFaceAnimation::handleEvent(Card * context, sf::Event event)
{
	//dummy
}

void BackToFaceAnimation::update(Card * context, sf::Time elapsedTime)
{
	animationTimer += elapsedTime;

	if (animationTimer.asMilliseconds() < 2*cfg::animationHalfTime.asMilliseconds())
	{
		if (animationTimer.asMilliseconds() < cfg::animationHalfTime.asMilliseconds())
		{
			//do only once
			currentGraphic = context->getBackGraphic();
			currentGraphic.scale(1.f - (animationTimer.asSeconds()/cfg::animationHalfTime.asSeconds()), 1);
		}
		else
		{
			//do only once
			currentGraphic = context->getFaceGraphic();
			currentGraphic.scale((animationTimer.asSeconds() - cfg::animationHalfTime.asSeconds()) / cfg::animationHalfTime.asSeconds(), 1);
		}
	}
	else
	{
		std::cout << "animation timer time:" << animationTimer.asMilliseconds() << std::endl;
		std::cout << "changing state to CardFace" << std::endl;
		changeState(context, new CardFace);
		
	}
}

void BackToFaceAnimation::render(Card * context, sf::RenderWindow * window)
{
	window->draw(currentGraphic, currentTransform);
}
